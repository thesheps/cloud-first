﻿using System.Threading.Tasks;
using App;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NUnit.Framework;

public class GivenHelloWorld
{
    [Test]
    public async Task WhenIGetHelloMessage_ThenMessageIsRetrieved()
    {
        var testServer = new TestServer(WebHost.CreateDefaultBuilder().UseStartup<Startup>());
        var client = testServer.CreateClient();
        var result = await client.GetStringAsync("/helloworld");

        Assert.That(result, Is.EqualTo("Hello World"));
    }
}